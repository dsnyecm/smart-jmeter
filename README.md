JMeter - SMART- WebSocket Samplers and Regression Report Generator
******************************************************************************************************************************************************
Building Jar:

The following command builds a jar file with all samplers, which then needs to be copied into JMeter installation ext directory:

	gradlew clean build -PjMeterExtDir="C:/apache-jmeter-2.13/lib/ext" -PsmartExtDir="C:/smart-jmeter/jmeter/build/jmeter/lib/ext"

******************************************************************************************************************************************************
Running JMeter script from the script's directory:

To run personnel load test use the following command:
	gradlew jmeterRun -PjmeterTestFiles="/scripts/smart2-loadTest/loadTest-smart2-personnel-WS.jmx" -Pdatafile="DATAFILE=/data/personnel/loadtest_personnel_group01_Loc1.csv" -PbaseUrl="BASE_URL=10.155.208.79" -Pport="PORT=80" -PuserName="USERNAME=admin" -Ppassword="PASSWORD=admin" -PloginHost="LOGIN_HOST=10.155.208.79" -PloginPort="LOGIN_PORT=8899" -Pusers="USERS=100" -Pduration="DURATION=7200" -Ploop="LOOP=1"


	To run equipment load test use the following command:
		gradlew jmeterRun -PjmeterTestFiles="/scripts/smart2-loadTest/loadTest-smart2-equipment-WS.jmx" -Pdatafile="DATAFILE=/data/equipment/loadtest_equipment_group01_loc1.csv" -PbaseUrl="BASE_URL=10.155.208.79" -Pport="PORT=80" -PuserName="USERNAME=admin" -Ppassword="PASSWORD=admin" -PloginHost="LOGIN_HOST=10.155.208.79" -PloginPort="LOGIN_PORT=8899" -Pusers="USERS=100" -Pduration="DURATION=7200" -Ploop="LOOP=1"
******************************************************************************************************************************************************
Generating Regression Report:

The report generation utility considers the oldest xml file in the[xml_report_directory_path] as a baseline, and the newest one as the latest report, and calculates regression time for each sampler by subtracting latest average from the baseline’s one. It also generates html file in the local GIT repository, and uploads it to remote GIT repository.

	gradlew run -PappArgs="['reportPath=c:/smart-jmeter/jmeter/build/jmeter-report', 'localGitPath=c:/smart-jmeter', 'jMeterScriptName=baseline', 'proxyServer=bcpxy.nycnet', 'proxyPort=8080', 'gitLogin=ipapkovdsny', 'gitPassword=3', 'pushToGit=true']"
******************************************************************************************************************************************************
Samplers Description:

	- CSV Smart Data Set – configuration plugin, which reads data from csv file, and transforms  all “offset-date” fields (column names ending with “date”) into actual dates (e.g., if now is 12/1/25 1:30 pm, then   -1~2~30 translates into yesterday 3:55 pm)
	- Smart WebSocket Connection Sampler – creates a web socket connection to the server
	- Smart WebSocket Subscriber Sampler – subscribes as a websocket listener
	- Smart WebSocket Rest Producer Sampler – sends the REST command  and receives websocket response
	- Smart WebSocket Producer Sampler – sends the websocket command  and receives websocket response
******************************************************************************************************************************************************