#!/bin/bash

set -v

testedUsers=${USERS:-10}
testedHost=${SMART_HOST:-msslvv-dsnsmt13.csc.nycnet}
authPort=${AUTH_PORT:-8899}


hostip=$(curl http://rancher-metadata.rancher.internal/2015-12-19/self/host/agent_ip)

hostlist=$(curl http://rancher-metadata.rancher.internal/2015-12-19/services/jmeterslave/containers|awk -F "=" '{print $2}' | while read -r line; do
     curl http://rancher-metadata.rancher.internal/2015-12-19/services/jmeterslave/containers/$line/primary_ip
     echo -n ","
done)


echo "hostlist=${hostlist}"


#cd /jmetermaster/apache-jmeter-2.13/bin

cd /jmetermaster/jmeter


echo EXECUTING: ./jmeter -n -t /jmetermaster/jmeter/scripts/smart2-loadTest/loadTest-smart2-allInOne-WS.jmx -JdataFile="DATAFILE=/data/loadtest_allInOne_groupXX.csv" -R $hostlist -Dclient.rmi.localport=1200 -Djava.rmi.server.hostname=${hostip}  -JbaseUrl="BASE_URL=${testedHost}" -Jport="PORT=80" -JuserName="USERNAME=users/usernames.csv" -Jpassword="PASSWORD=Change4dsny" -JloginHost="LOGIN_HOST=${testedHost}" -JloginPort="LOGIN_PORT=${authPort}" -Jusers="USERS=${testedUsers}" -Jduration="DURATION=7200"


#echo EXECUTING: ./jmetermaster/jmeter/scripts/smart2-loadTest/loadTest-smart2-equipment-WS.jmx -R $hostlist -JdataFile="DATAFILE=/data/loadtest_equipment_groupXX.csv"  -JbaseUrl="BASE_URL=${testedHost}" -Jport="PORT=80" -JuserName="USERNAME=/data/usernames.csv" -Jpassword="PASSWORD=Change4dsny" -JloginHost="LOGIN_HOST=${testedHost}" -JloginPort="LOGIN_PORT=${authPort}" -Jusers="USERS=${testedUsers}" -Jduration="DURATION=3600"


#echo EXECUTING: ./jmetermaster/jmeter/scripts/smart2-loadTest/loadTest-smart2-personnel-WS.jmx -R $hostlist -JdataFile="DATAFILE=/data/loadtest_equipment_groupXX.csv"  -JbaseUrl="BASE_URL=${testedHost}" -Jport="PORT=80" -JuserName="USERNAME=/data/usernames.csv" -Jpassword="PASSWORD=Change4dsny" -JloginHost="LOGIN_HOST=${testedHost}" -JloginPort="LOGIN_PORT=${authPort}" -Jusers="USERS=${testedUsers}" -Jduration="DURATION=3600"


#echo EXECUTING: ./jmetermaster/jmeter/scripts/smart2-loadTest/loadTest-smart2-tasks-WS.jmx -R $hostlist -JdataFile="DATAFILE=/data/loadtest_equipment_group01.csv"  -JbaseUrl="BASE_URL=${testedHost}" -Jport="PORT=80" -JuserName="USERNAME=/data/usernames.csv" -Jpassword="PASSWORD=Change4dsny" -JloginHost="LOGIN_HOST=${testedHost}" -JloginPort="LOGIN_PORT=${authPort}" -Jusers="USERS=${testedUsers}" -Jduration="DURATION=3600"


ls -la /jmetermaster/jmeter/scripts/smart2-loadTest/


#cd /jmetermaster/apache-jmeter-2.13/bin

cd /jmetermaster/jmeter

./gradlew jmeterRun -PjmeterTestFiles="scripts/smart2-loadTest/loadTest-smart2-allInOne-WS.jmx" -PdataFile="DATAFILE=data/loadtest_allInOne_groupXX.csv" -PbaseUrl="BASE_URL=${testedHost}" -Pport="PORT=80" -PuserName="USERNAME=data/usernames.csv" -Ppassword="PASSWORD=Change4dsny" -PloginHost="LOGIN_HOST=${testedHost}" -PloginPort="LOGIN_PORT=8899" -Pusers="USERS=10" -Ploop="LOOP=1" -Pduration="DURATION=7200"


#./jmeter -n -t /jmetermaster/jmeter/scripts/smart2-loadTest/loadTest-smart2-allInOne-WS.jmx -JdataFile="DATAFILE=/data/loadtest_allInOne_groupXX.csv" -R $hostlist -Dclient.rmi.localport=1200 -Djava.rmi.server.hostname=${hostip}  -JbaseUrl="BASE_URL=${testedHost}" -Jport="PORT=80" -JuserName="USERNAME=users/usernames.csv" -Jpassword="PASSWORD=Change4dsny" -JloginHost="LOGIN_HOST=${testedHost}" -JloginPort="LOGIN_PORT=${authPort}" -Jusers="USERS=${testedUsers}" -Jduration="DURATION=7200"

#./jmeter -n -t /jmetermaster/jmeter/scripts/smart2-loadTest/loadTest-smart2-equipment-WS.jmx -JdataFile="DATAFILE=/data/loadtest_equipment_groupXX.csv" -R $hostlist -Dclient.rmi.localport=1200 -Djava.rmi.server.hostname=${hostip}  -JbaseUrl="BASE_URL=${testedHost}" -Jport="PORT=80" -JuserName="USERNAME=users/usernames.csv" -Jpassword="PASSWORD=Change4dsny" -JloginHost="LOGIN_HOST=${testedHost}" -JloginPort="LOGIN_PORT=${authPort}" -Jusers="USERS=${testedUsers}" -Jduration="DURATION=7200"


#./jmeter -n -t /jmetermaster/jmeter/scripts/smart2-loadTest/loadTest-smart2-personnel-WS.jmx -JdataFile="DATAFILE=/data/loadtest_personnel_groupXX.csv" -R $hostlist -Dclient.rmi.localport=1200 -Djava.rmi.server.hostname=${hostip}  -JbaseUrl="BASE_URL=${testedHost}" -Jport="PORT=80" -JuserName="USERNAME=users/usernames.csv" -Jpassword="PASSWORD=Change4dsny" -JloginHost="LOGIN_HOST=${testedHost}" -JloginPort="LOGIN_PORT=${authPort}" -Jusers="USERS=${testedUsers}" -Jduration="DURATION=7200"


#./jmeter -n -t /jmetermaster/jmeter/scripts/smart2-loadTest/loadTest-smart2-tasks-WS.jmx -JdataFile="DATAFILE=/data/loadtest_tasks_groupXX.csv" -R $hostlist -Dclient.rmi.localport=1200 -Djava.rmi.server.hostname=${hostip}  -JbaseUrl="BASE_URL=${testedHost}" -Jport="PORT=80" -JuserName="USERNAME=users/usernames.csv" -Jpassword="PASSWORD=Change4dsny" -JloginHost="LOGIN_HOST=${testedHost}" -JloginPort="LOGIN_PORT=${authPort}" -Jusers="USERS=${testedUsers}" -Jduration="DURATION=7200"

ls -la /jmetermaster/jmeter/build/jmeter-report/

tar -zcvf /tmp/jmeter-report${AUTO_BUILD_NUMBER}.tar.gz /jmetermaster/jmeter/build/jmeter-report/

curl -F "myfiles=@/tmp/jmeter-report${AUTO_BUILD_NUMBER}.tar.gz" ${REPORT_URL}

lighttpd -t -f /tmp/lighttpd.conf
lighttpd -D -f /tmp/lighttpd.conf

sleep 1800
