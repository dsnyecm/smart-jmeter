#!/bin/bash

set -v

hostip=$(curl http://rancher-metadata.rancher.internal/2015-12-19/self/host/agent_ip)

export RMI_HOST_DEF=" -Djava.rmi.server.hostname=${hostip} -Dserver.rmi.localport=1100 "

echo RMI_HOST_DEF=${RMI_HOST_DEF}

curl http://rancher-metadata.rancher.internal/2015-12-19/self/container/create_index

currentnode=$(curl http://rancher-metadata.rancher.internal/2015-12-19/self/container/create_index)

cd /jmeterserver/group${currentnode}/ 

pwd

/jmeterserver/apache-jmeter-2.13/bin/jmeter-server &

lighttpd -t -f /tmp/lighttpd.conf
lighttpd -D -f /tmp/lighttpd.conf &

tail -F /jmeterserver/group${currentnode}/jmeter-server.log
