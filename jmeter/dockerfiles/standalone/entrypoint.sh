#!/bin/bash

set -v
set +e

testedUsers=${USERS:-1}
testedHost=${SMART_HOST:-"msslvv-dsnsmt13.csc.nycnet"}
edgePort=${EDGE_PORT:-80}
authPort=${AUTH_PORT:-8899}
testLoop=${LOOPS:-1}
testDuration=${DURATION:-7200}
TestTimeout=${TIMEOUT:-15000}

curl http://rancher-metadata.rancher.internal/2015-12-19/self/container/create_index
currentnode=$(curl http://rancher-metadata.rancher.internal/2015-12-19/self/container/create_index)


cd /jmeter


echo jmeterTestFiles=scripts/smart2-loadTest/loadTest-smart2-allInOne-WS.jmx >> jmeter.properties
echo dataFile="DATAFILE=data/loadtest_allInOne_group0${currentnode}.csv" >> jmeter.properties
echo baseUrl="BASE_URL=${testedHost}" >> jmeter.properties
echo port="PORT=${edgePort}" >> jmeter.properties
echo userName="USERNAME=data/usernames${currentnode}.csv" >> jmeter.properties
echo loginHost="LOGIN_HOST=${testedHost}" >> jmeter.properties
echo loginPort="LOGIN_PORT=${authPort}" >> jmeter.properties
echo users="USERS=${testedUsers}" >> jmeter.properties
echo loop="LOOP=${testLoop}" >> jmeter.properties
echo duration="DURATION=${testDuration}" >> jmeter.properties
echo timeout="RESPONSE_TIMEOUT=${TestTimeout}" >> jmeter.properties


echo DATAFILE=data/loadtest_allInOne_group0${currentnode}.csv >> jmeter.properties
echo BASE_URL=${testedHost} >> jmeter.properties
echo PORT=${edgePort} >> jmeter.properties
echo USERNAME=data/usernames${currentnode}.csv >> jmeter.properties
echo LOGIN_HOST=${testedHost} >> jmeter.properties
echo LOGIN_PORT=${authPort} >> jmeter.properties
echo USERS=${testedUsers} >> jmeter.properties
echo LOOP=${testLoop} >> jmeter.properties
echo DURATION=${testDuration} >> jmeter.properties
echo RESPONSE_TIMEOUT=${TestTimeout} >> jmeter.properties


pwd

if [[ -n $PROXY_USER ]]; then
   echo systemProp.http.proxyUser=$PROXY_USER >> gradle.properties
   echo systemProp.https.proxyUser=$PROXY_USER >> gradle.properties
fi

if [[ -n $PROXY_PASSWORD ]]; then
   echo systemProp.http.proxyPassword=$PROXY_PASSWORD >> gradle.properties
   echo systemProp.https.proxyPassword=$PROXY_PASSWORD >> gradle.properties
fi

echo




cmnd="./gradlew jmeterRun -PjmeterTestFiles=scripts/smart2-loadTest/loadTest-smart2-allInOne-WS.jmx -PdataFile=\"DATAFILE=data/loadtest_allInOne_group0${currentnode}.csv\" -PbaseUrl=\"BASE_URL=${testedHost}\" -Pport=\"PORT=${edgePort}\" -PuserName=\"USERNAME=data/usernames${currentnode}.csv\" -PloginHost=\"LOGIN_HOST=${testedHost}\" -PloginPort=\"LOGIN_PORT=${authPort}\" -Pusers=\"USERS=${testedUsers}\" -Ploop=\"LOOP=${testLoop}\" -Pduration=\"DURATION=${testDuration}\" -Ptimeout=\"RESPONSE_TIMEOUT=${TestTimeout}\""


echo ${cmnd}
exec ${cmnd}|tee /dev/null

tar -zcvf /jmeter/build/jmeter/jmeter.tar.gz /jmeter/build/jmeter/jmeter.log

lighttpd -t -f /tmp/lighttpd.conf
lighttpd -D -f /tmp/lighttpd.conf