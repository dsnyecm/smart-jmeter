/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmeter.plugins.samplers.smart;

import jmeter.plugins.samplers.smart.websocket.subscriber.SmartWebSocketSubscriberSessionHandler;
import org.apache.commons.lang3.StringUtils;
import org.apache.jmeter.config.Argument;
import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.protocol.http.control.CookieManager;
import org.apache.jmeter.protocol.http.sampler.HTTPSampler;
import org.apache.jmeter.protocol.http.util.EncoderCache;
import org.apache.jmeter.protocol.http.util.HTTPArgument;
import org.apache.jmeter.protocol.http.util.HTTPConstants;
import org.apache.jmeter.samplers.Entry;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.testelement.TestElement;
import org.apache.jmeter.testelement.TestStateListener;
import org.apache.jmeter.testelement.property.PropertyIterator;
import org.apache.jmeter.testelement.property.StringProperty;
import org.apache.jmeter.testelement.property.TestElementProperty;
import org.apache.jmeter.threads.JMeterContext;
import org.apache.jmeter.threads.JMeterContextService;
import org.apache.jmeter.threads.JMeterVariables;
import org.apache.jmeter.util.JMeterUtils;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.jorphan.util.JOrphanUtils;
import org.apache.log.Logger;
import org.springframework.messaging.simp.stomp.ConnectionLostException;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.util.MultiValueMap;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


public abstract class SmartWebSocketAbstractSampler extends HTTPSampler implements TestStateListener {
    private static final long serialVersionUID = 1L;

    private static final Logger log = LoggingManager.getLoggerForClass();

    protected HashSet<String> subscriptionTo = new HashSet<String>();

    private static final Logger logger = LoggingManager.getLoggerForClass();

    protected static final String ARG_VAL_SEP = "="; // $NON-NLS-1$
    protected static final String QRY_SEP = "&"; // $NON-NLS-1$
    protected static final String WS_PREFIX = "ws://"; // $NON-NLS-1$
    protected static final String WSS_PREFIX = "wss://"; // $NON-NLS-1$
    protected static final String DEFAULT_PROTOCOL = "ws";

    private static String WEBSOCKET_SESSION_KEY = "WEBSOCKET_SESSION";
    private static String SUBSCRIBER_HANDLER_KEY = "SUBSCRIBER_HANDLER";
    private static String JSESSIONID_KEY = "JSESSIONID";
    private static String CSRF_KEY = "CSRF";
    private static String TASK_SCHEDULER_KEY = "TASK_SCHEDULER_KEY";
    private static final int TIMEOUT_RETRIES = 30;
    private static final int DEFAULT_RESPONSE_TIMEOUT = 10 * 1000;
    private static String ERROR_PREFIX = "********* Error *********** ";

    protected SmartWebSocketAbstractSampler() {
        super();
    }

    @Override
    public abstract SampleResult sample(Entry entry);

    protected void initializeSampleResult(SampleResult sampleResult,
                                          StringBuilder errorList, MultiValueMap<String, String> headers) {
        sampleResult.setSampleLabel(getName());
        sampleResult.setDataEncoding(getContentEncoding());
        sampleResult.setRequestHeaders(serializeHeaders(headers));
        return;
    }

    protected void initializeHeaders(MultiValueMap<String, String> headers) {
        headers.add("Accept-Encoding", "gzip, deflate, sdch");
        headers.add("Accept-Language", "en-US,en;q=0.8");
        headers.add("Cache-Control", "no-cache");
        headers.add("Connection", "Upgrade");
        headers.add("Host", getServerAddressAndPort());
        headers.add("Origin", ("http://" + getServerAddressAndPort()));
        headers.add("Referer", ("http://" + getServerAddressAndPort()) + "/ui/");
        headers.add("Pragma", "no-cache");
        headers.add("Sec-WebSocket-Extensions", "permessage-deflate; client_max_window_bits");
        headers.add("Sec-WebSocket-Key", "Flsw69Bcxm5DMpByPMv1aQ==");
        headers.add("Sec-WebSocket-Version", "13");
        headers.add("Upgrade", "websocket");
        headers.add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36");

        String cookieHeader = null;
        CookieManager cookieManager = this.getCookieManager();
        if (cookieManager != null) {
            try {
                cookieHeader = cookieManager.getCookieHeaderForURL(new URL("http://" + this.getServerAddressAndPort()));
                if (cookieHeader != null) {
                    headers.add("Cookie", cookieHeader.toString());

                    String[] cookies = cookieHeader.toString().split(";");
                    for (String cookie : cookies) {
                        if (cookie.contains("XSRF-TOKEN")) {
                            int index = cookie.indexOf("=") + 1;
                            headers.add("X-XSRF-TOKEN", cookie.substring(index));
                        }
                    }

                }
            } catch (Exception e) {
                log.warn("Cookie manager error", e);
            }
        }

        return;
    }

    protected static StompSession getStompSession() {
        StompSession session = null;
        for (int i = 0; i < 10; i++) {
            session = (StompSession) getVariable(WEBSOCKET_SESSION_KEY);
            if (session == null)
                sleep(1000L);
        }
        return session;
    }

    protected static String getUserName() {
        return (String) getVariable("USERNAME");
    }

    public static void registerStompSession(StompSession session) {
        setVariable(WEBSOCKET_SESSION_KEY, session);
        return;
    }


    protected static SmartWebSocketSubscriberSessionHandler getSubscriberHandler() {
        SmartWebSocketSubscriberSessionHandler stompSubscriberSessionHandler = (SmartWebSocketSubscriberSessionHandler) getVariable(SUBSCRIBER_HANDLER_KEY);
        return stompSubscriberSessionHandler;
    }

    protected static void registerSubscriberHandler(SmartWebSocketSubscriberSessionHandler stompSubscriberSessionHandler) {

        setVariable(SUBSCRIBER_HANDLER_KEY, stompSubscriberSessionHandler);
        return;
    }


    protected static ThreadPoolTaskScheduler getThreadPoolTaskScheduler() {
        ThreadPoolTaskScheduler taskScheduler = (ThreadPoolTaskScheduler) getVariable(TASK_SCHEDULER_KEY);
        return taskScheduler;
    }

    protected static void registerThreadPoolTaskScheduler(ThreadPoolTaskScheduler taskScheduler) {

        setVariable(TASK_SCHEDULER_KEY, taskScheduler);
        return;
    }


    protected static String getJSessionId() {
        String jSessionId = (String) getVariable(JSESSIONID_KEY);
        return jSessionId;
    }

    protected static void registerJSessionId(String jSessionId) {

        setVariable(JSESSIONID_KEY, jSessionId);
        return;
    }

    protected static String getCsrf() {
        String jSessionId = (String) getVariable(CSRF_KEY);
        return jSessionId;
    }

    protected static void registerCsrf(String csrf) {
        setVariable(CSRF_KEY, csrf);
        return;
    }


    private static Object getVariable(String key) {
        JMeterContext jmetercontext = JMeterContextService.getContext();
        JMeterVariables vars = jmetercontext.getVariables();
        return vars.getObject(key);
    }

    protected static void setVariable(String key, Object obj) {
        JMeterContext jmetercontext = JMeterContextService.getContext();
        JMeterVariables vars = jmetercontext.getVariables();
        if (obj != null)
            vars.putObject(key, obj);
        else
            vars.remove(key);
        jmetercontext.setVariables(vars);
    }

    @Override
    public void setName(String name) {
        if (name != null) {
            setProperty(TestElement.NAME, name);
        }
    }

    @Override
    public String getName() {
        return getPropertyAsString(TestElement.NAME);
    }

    @Override
    public void setComment(String comment) {
        setProperty(new StringProperty(TestElement.COMMENTS, comment));
    }

    @Override
    public String getComment() {
        return getProperty(TestElement.COMMENTS).getStringValue();
    }

    public URI getUri() throws URISyntaxException {
        String path = this.getContextPath();
        // Hack to allow entire URL to be provided in host field
        if (path.startsWith(WS_PREFIX)
                || path.startsWith(WSS_PREFIX)) {
            return new URI(path);
        }
        String domain = getServerAddress();
        String protocol = getProtocol();
        // HTTP URLs must be absolute, allow file to be relative
        if (!path.startsWith("/")) { // $NON-NLS-1$
            path = "/" + path; // $NON-NLS-1$
        }

        String queryString = getQueryString(getContentEncoding());
        if (isProtocolDefaultPort()) {
            return new URI(protocol, null, domain, -1, path, queryString, null);
        }
        return new URI(protocol, null, domain, Integer.parseInt(getServerPort()), path, queryString, null);
    }

    /**
     * Tell whether the default port for the specified protocol is used
     *
     * @return true if the default port number for the protocol is used, false
     * otherwise
     */
    public boolean isProtocolDefaultPort() {
        final int port = Integer.parseInt(getServerPort());
        final String protocol = getProtocol();
        return ("ws".equalsIgnoreCase(protocol) && port == HTTPConstants.DEFAULT_HTTP_PORT)
                || ("wss".equalsIgnoreCase(protocol) && port == HTTPConstants.DEFAULT_HTTPS_PORT);
    }

    public String getServerPort() {
        return getPropertyAsString("serverPort", "");
    }

    public void setServerPort(String port) {
        setProperty("serverPort", port);
    }


    public void setResponseTimeout(String responseTimeout) {
        setProperty("responseTimeout", responseTimeout);
    }

    private int getCommandTimeout() {
        String stringTimeout =  (String) getVariable ("RESPONSE_TIMEOUT");
        return StringUtils.isBlank(stringTimeout)? DEFAULT_RESPONSE_TIMEOUT : Integer.parseInt(stringTimeout);
    }

    public String getConnectionTimeout() {
        return getPropertyAsString("connectionTimeout", "5000");
    }

    public void setConnectionTimeout(String connectionTimeout) {
        setProperty("connectionTimeout", connectionTimeout);
    }

    public void setProtocol(String protocol) {
        setProperty("protocol", protocol);
    }

    public String getProtocol() {
        String protocol = getPropertyAsString("protocol");
        if (protocol == null || protocol.isEmpty()) {
            return DEFAULT_PROTOCOL;
        }
        return protocol;
    }

    public void setServerAddress(String serverAddress) {
        setProperty("serverAddress", serverAddress);
    }

    public String getServerAddress() {
        return getPropertyAsString("serverAddress");
    }


    public void setImplementation(String implementation) {
        setProperty("implementation", implementation);
    }

    public String getImplementation() {
        return getPropertyAsString("implementation");
    }

    public void setContextPath(String contextPath) {
        setProperty("contextPath", contextPath);
    }

    public String getContextPath() {
        return getPropertyAsString("contextPath");
    }

    public void setContentEncoding(String contentEncoding) {
        setProperty("contentEncoding", contentEncoding);
    }

    public String getContentEncoding() {
        return getPropertyAsString("contentEncoding", "UTF-8");
    }

    public void setRequestPayload(String requestPayload) {
        setProperty("requestPayload", requestPayload);
    }

    public String getRequestPayload() {
        return getPropertyAsString("requestPayload");
    }

    public void setIgnoreSslErrors(Boolean ignoreSslErrors) {
        setProperty("ignoreSslErrors", ignoreSslErrors);
    }

    public Boolean isIgnoreSslErrors() {
        return getPropertyAsBoolean("ignoreSslErrors");
    }

    public void setConnectionId(String connectionId) {
        setProperty("connectionId", connectionId);
    }

    public String getConnectionId() {
        return getPropertyAsString("connectionId");
    }

    public void setResponsePattern(String responsePattern) {
        setProperty("responsePattern", responsePattern);
    }

    public String getResponsePattern() {
        return getPropertyAsString("responsePattern");
    }

    public void setCloseConncectionPattern(String closeConncectionPattern) {
        setProperty("closeConncectionPattern", closeConncectionPattern);
    }

    public String getCloseConncectionPattern() {
        return getPropertyAsString("closeConncectionPattern");
    }

    public void setProxyAddress(String proxyAddress) {
        setProperty("proxyAddress", proxyAddress);
    }

    public String getProxyAddress() {
        return getPropertyAsString("proxyAddress");
    }

    public void setProxyPassword(String proxyPassword) {
        setProperty("proxyPassword", proxyPassword);
    }

    public String getProxyPassword() {
        return getPropertyAsString("proxyPassword");
    }

    public void setProxyPort(String proxyPort) {
        setProperty("proxyPort", proxyPort);
    }

    public String getProxyPort() {
        return getPropertyAsString("proxyPort");
    }

    public void setProxyUsername(String proxyUsername) {
        setProperty("proxyUsername", proxyUsername);
    }

    public String getProxyUsername() {
        return getPropertyAsString("proxyUsername");
    }

    public void setMessageBacklog(String messageBacklog) {
        setProperty("messageBacklog", messageBacklog);
    }

    public String getMessageBacklog() {
        return getPropertyAsString("messageBacklog", "3");
    }


    public String getQueryString(String contentEncoding) {
        // Check if the sampler has a specified content encoding
        if (JOrphanUtils.isBlank(contentEncoding)) {
            // We use the encoding which should be used according to the HTTP spec, which is UTF-8
            contentEncoding = EncoderCache.URL_ARGUMENT_ENCODING;
        }
        StringBuilder buf = new StringBuilder();
        PropertyIterator iter = getQueryStringParameters().iterator();
        boolean first = true;
        while (iter.hasNext()) {
            HTTPArgument item = null;
            Object objectValue = iter.next().getObjectValue();
            try {
                item = (HTTPArgument) objectValue;
            } catch (ClassCastException e) {
                item = new HTTPArgument((Argument) objectValue);
            }
            final String encodedName = item.getEncodedName();
            if (encodedName.length() == 0) {
                continue; // Skip parameters with a blank name (allows use of optional variables in parameter lists)
            }
            if (!first) {
                buf.append(QRY_SEP);
            } else {
                first = false;
            }
            buf.append(encodedName);
            if (item.getMetaData() == null) {
                buf.append(ARG_VAL_SEP);
            } else {
                buf.append(item.getMetaData());
            }

            // Encode the parameter value in the specified content encoding
            try {
                buf.append(item.getEncodedValue(contentEncoding));
            } catch (UnsupportedEncodingException e) {
                logger.warn("Unable to encode parameter in encoding " + contentEncoding + ", parameter value not included in query string");
            }
        }
        return buf.toString();
    }

    public void setQueryStringParameters(Arguments queryStringParameters) {
        setProperty(new TestElementProperty("queryStringParameters", queryStringParameters));
    }

    public Arguments getQueryStringParameters() {
        Arguments args = (Arguments) getProperty("queryStringParameters").getObjectValue();
        return args;
    }


    @Override
    public void testStarted() {
        testStarted("unknown");
    }

    @Override
    public void testStarted(String host) {
        //connectionList = new ConcurrentHashMap<String, ServiceSocket>();
    }

    @Override
    public void testEnded() {
        testEnded("unknown");
    }

    @Override
    public void testEnded(String host) {

    }

    protected static void sleep(long ms) {
        logger.info("Sleeping " + ms);
        try {
            Thread.sleep(ms);
        } catch (Exception e) {
        }
    }

    public String getServerAddressAndPort() {
        StringBuilder sb = new StringBuilder();
        sb.append(getServerAddress());

        String port = getServerPort();
        if (StringUtils.isNotBlank(port) && !"80".equals(port))
            sb.append(":").append(getServerPort());

        return sb.toString();

    }
    
    private String serializeSet(Set<String> set)
    {
    	StringBuilder sb = new StringBuilder();
    	for (String s : set)
    	{
    		sb.append(s).append("~~~");
    	}
    	return sb.toString();
    }
    
    protected String serializeHeaders(MultiValueMap<String, String> headers)
    {
    	StringBuilder serializedHeaders = new StringBuilder();
    	
    	for (Map.Entry<String, List<String>> entry : headers.entrySet())
    	{
    		serializedHeaders.append("\n").append(entry.getKey()).append(":");
    		for (String value : entry.getValue())
    		{
    			serializedHeaders.append(value).append(", ");
    		}
    	}
    	
    	return serializedHeaders.toString();
    }
    
    protected String addUniqueIdToRequest (String bodyWithoutId, String uniqueId)
    {
        int index = bodyWithoutId.lastIndexOf("}");
        StringBuilder bodyWithId = new StringBuilder(bodyWithoutId.substring(0, index));
        bodyWithId.append(", \"commandId\":\"").append(uniqueId).append("\"}");
        return bodyWithId.toString();
    }

    protected boolean getWebSocketResponse(String uniqueId, SampleResult sampleResult, StringBuilder logMessage,
        StringBuilder errorList, Map<String, String[]> expectedLocationsMap, StompSession session ) {
        boolean isOK = true;
        boolean allMessagesReceived = false;
        Set<String> locationSubscriptions = null;

        try
        {
            SmartWebSocketSubscriberSessionHandler stompSubscriberSessionHandler = getSubscriberHandler();
            ActionHolder ah = stompSubscriberSessionHandler.getActionHolder(uniqueId, null, -1, null);
            int retires = getCommandTimeout() / 1000;
            log.debug("Number of retries *************************************** " + retires);
            for (int j = 0; j < retires; j++)
            {
                log.debug("Waiting for " + j + "seconds...");

                int counter = 1;
                while (true) {
                    String otherThreadMessage = JMeterUtils.getProperty(SmartWebSocketSubscriberSessionHandler.getPropertyName(uniqueId, counter));
                    if ("done".equals(otherThreadMessage))
                    {
                        counter +=1;
                        continue;
                    }

                    if (otherThreadMessage != null) {
                        String[] parts = otherThreadMessage.split(SmartWebSocketSubscriberSessionHandler.getPropertyDelimiter());
                        JMeterUtils.setProperty(SmartWebSocketSubscriberSessionHandler.getPropertyName(uniqueId, counter), "done");

                        log.debug("************ Processing message which was received by another thread + { " + otherThreadMessage + " }");
                        if (ah.getMessage() == null) {
                            ah.setMessage(parts[0]);
                            ah.setEndTime(Long.parseLong(parts[1]));
                            sampleResult.setEndTime(ah.getEndTime());
                            sampleResult.setResponseData(ah.getMessage().getBytes());
                            logMessage.append(ah.getMessage());
                        }
                        SmartWebSocketSubscriberSessionHandler.finalizeActionHolderState(ah, ah.getMessage(), ah.getEndTime(), parts[2]);
                        counter +=1;
                    }
                    else
                    {
                        break;
                    }
                }



                if (session != null && !session.isConnected()) {
                    String errorMessage = " Session " + session.getSessionId() + " got disconnected, so no more events are expected for [" + uniqueId + "][" + getUserName() + "]";
                    throw new ConnectionLostException(errorMessage);
                }

                if (ah.getMessage() != null)
                {
                    if (ah.isError()) {
                        isOK = false;
                        String text = "NoOp message came back for command " + uniqueId + ", message: " + ah.getMessage();
                        log.debug(text);
                        logMessage.insert(0, text);
                        sampleResult.setResponseData(ah.getMessage().getBytes());
                        sampleResult.setEndTime(ah.getEndTime());
                        errorList.append(text);
                        break;
                    }
                    else {
                        if (isIgnoreSslErrors()) {
                            locationSubscriptions = stompSubscriberSessionHandler.getSubscribedLocations(expectedLocationsMap);
                            logger.debug("**************** Message came from: " + serializeSet(locationSubscriptions));

                            Set<String> destinations = ah.getDestinations();
                            logger.debug("**************** ActionHolder' subscriptions: " + serializeSet(destinations));
                            Set<String> modifiedDestinations = new HashSet<String>();
                            for (String destination : destinations) {
                                if (destination == null)
                                    continue;

                                for (String locationSubscription : locationSubscriptions) {
                                    if (destination.contains(locationSubscription)) {
                                        modifiedDestinations.add(locationSubscription);
                                    }
                                }

                            }


                            if (modifiedDestinations.containsAll(locationSubscriptions)) {
                                logger.debug("*************** All messages are received for ActionHolder " + ah.getUniqueId());
                                logMessage.append(ah.getMessage());
                                sampleResult.setResponseData(ah.getMessage().getBytes());
                                sampleResult.setEndTime(ah.getEndTime());

                                allMessagesReceived = true;
                                break;
                            } else {
                                logger.debug("*************** Not all messages are received for ActionHolder  after " + retires  +" seconds for command " + ah.getUniqueId());
                            }
                        } else {
                            logger.debug("*************** Not waiting for message for ActionHolder " + ah.getUniqueId());
                            sampleResult.setResponseData(ah.getMessage().getBytes());
                            sampleResult.setEndTime(ah.getEndTime());
                            logMessage.append(ah.getMessage());
                            break;
                        }
                    }
                }
                sleep(1000);
            }

            if (ah.getMessage() == null)
            {
                isOK = false;

                String message = ERROR_PREFIX + "No messages were received after " + retires + " seconds ( for commandId '" + ah.getUniqueId() + "' and userName '" + getUserName() + "')\n";
                log.error(message);
                logMessage.insert(0, message);
                sampleResult.setResponseData(message.getBytes());
                sampleResult.setEndTime(System.currentTimeMillis());
                errorList.append(message);
            }
            else
            {
                sampleResult.setEndTime(ah.getEndTime());
                StringBuilder sb = new StringBuilder(ah.getMessage());
                if (!allMessagesReceived) {
                    String text = "Not all message for all destinations were recieved for command " + uniqueId + " Received message only from " + ah.getDestinationsAsString();
                    log.warn(text);
                    sb.insert(0, text);
                    isOK = false;
                }
                sampleResult.setResponseData(sb.toString().getBytes());
                logMessage.append(sb.toString());
            }
        }
        catch (Throwable e)
        {
            isOK = false;
            String message = ERROR_PREFIX + "while waiting for results for command " + uniqueId + " with error:" + e.getMessage();
            log.error(message, e);
            logMessage.insert(0, message);
            sampleResult.setResponseData(message.getBytes());
            sampleResult.setEndTime(System.currentTimeMillis());
        }

        return isOK;
    }

    protected String addUniqueIdToRequest(String bodyWithoutId, String uniqueId, String commandPath) {
        int index = bodyWithoutId.lastIndexOf("}");
        StringBuilder bodyWithId = new StringBuilder(bodyWithoutId.substring(0, index));
        bodyWithId.append(", \"commandId\":\"").append(uniqueId).append("\", \"userName\":\"").append(getUserName())
                .append("\", \"commandPath\":\"").append(commandPath).append("\"}");
        return bodyWithId.toString();
    }

    protected void populateExpectedLocationsMap(Map<String, String[]> expectedLocationsMap) {
        String responsePattern = getResponsePattern();
        String[] domains = responsePattern.split(";");
        for (String domain : domains) {
            String[] domainParts = domain.split("=");
            String[] locations = domainParts[1].split(",");
            expectedLocationsMap.put(domainParts[0], locations);
            log.debug("*********************** Setting expected locations for " + domainParts[0] + " as " + domainParts[1]);
        }
    }

}
