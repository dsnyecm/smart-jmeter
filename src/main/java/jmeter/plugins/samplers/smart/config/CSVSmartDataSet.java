package jmeter.plugins.samplers.smart.config;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Map.Entry;

import org.apache.jmeter.config.CSVDataSet;
import org.apache.jmeter.engine.event.LoopIterationEvent;
import org.apache.jmeter.threads.JMeterContext;
import org.apache.jmeter.threads.JMeterVariables;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;

public class CSVSmartDataSet extends CSVDataSet
{
	private static final Logger log = LoggingManager.getLoggerForClass();
	
	 @Override
	 public void iterationStart(LoopIterationEvent iterEvent) 
	 {
		 super.iterationStart(iterEvent);
		 
		 final JMeterContext context = getThreadContext();
		 JMeterVariables threadVars = context.getVariables();
		 
		 Iterator<Entry<String, Object>> itr = threadVars.getIterator();
		 while (itr.hasNext())
		 {
			 Entry<String, Object> variable = itr.next();
			 String varName = variable.getKey();
			 if (varName.toLowerCase().endsWith("date"))
			 {
				 threadVars.put(varName, processDate(varName, variable.getValue().toString()));
			 }
		 }
	 }
	 
	 // Date formats
	 // offset date ~ offset hours ~ offest minutes    0~1~30, 3~-1~-30, -1~-3~30
	 private String processDate (String name, String date)
	 {
		 if (date.length() == 10 && date.contains("-"))
		 {
			 // Do not change boardDate
			 return date;
		 }

		 
		 if (date.contains("~"))
			 return processTildaDate (name, date);
		 else if (date.contains(":"))
			 return processColonDate (name, date);
		 else
		 {
			 RuntimeException e = new RuntimeException ("Date format is unknown " + date);
			 log.error("Date format error", e);
			 throw e;
		 }
	 }
	 
	 private String processTildaDate (String name, String date)
	 {

		 String[] parts = date.split("~");
		 if (parts.length != 3)
		 {
			 RuntimeException e = new RuntimeException ("Date format is expected as 'offset date ~ offset hours ~ offset minutes' but came as " + date);
			 log.error("Date format error", e);
			 throw e;
			 
		 }
		 
		 Calendar today = Calendar.getInstance();
		 today.add(Calendar.DATE, Integer.parseInt(parts[0]));
		 today.add(Calendar.HOUR_OF_DAY, Integer.parseInt(parts[1]));
		 today.add(Calendar.MINUTE, Integer.parseInt(parts[2]));
		 
		 SimpleDateFormat format;
		 if (name.toLowerCase().endsWith("boarddate"))
		 {
			 format = new SimpleDateFormat("yyyy-MM-dd"); 
		 }else if (name.equalsIgnoreCase("shiftStartDate") || name.equalsIgnoreCase("shiftEndDate"))
		 {
			 format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm"); 
		 }
		 else
		 {
			 format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		 }

		 date = format.format(today.getTime()); 
			 
		 log.debug("*************** " + name + " : " + date + " **********************");
		 
		 return date;
	 }
	 
	 private String processColonDate (String name, String date)
	 {

		 String[] parts = date.split(":");
		 if (parts.length != 3)
		 {
			 RuntimeException e = new RuntimeException ("Date format is expected as 'offset date : offset hours : offset minutes' but came as " + date);
			 log.error("Date format error", e);
			 throw e;
		 }
		 
		 
		 Calendar today = Calendar.getInstance();
		 today.add(Calendar.DATE, Integer.parseInt(parts[0]));
		 today.set(Calendar.HOUR_OF_DAY, Integer.parseInt(parts[1]));
		 today.set(Calendar.MINUTE, Integer.parseInt(parts[2]));
		 today.set(Calendar.SECOND, 0);
		 today.set(Calendar.MILLISECOND, 0);
		 
		 SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		 date = format.format(today.getTime()); 
			 
		 log.debug("*************** " + name + " : " + date + " **********************");
		 
		 return date;
	 }
}
