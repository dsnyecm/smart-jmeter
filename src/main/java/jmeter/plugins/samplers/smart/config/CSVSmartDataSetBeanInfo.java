package jmeter.plugins.samplers.smart.config;

import org.apache.jmeter.testbeans.BeanInfoSupport;

public class CSVSmartDataSetBeanInfo extends BeanInfoSupport 
{
	public CSVSmartDataSetBeanInfo() 
	{
		super(CSVSmartDataSet.class);
	}

}
