package jmeter.plugins.samplers.smart;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * Created by vgellerman on 12/4/2015.
 */
public class SMARTObjectMapper extends ObjectMapper{

	public SMARTObjectMapper(){
		setSerializationInclusion(JsonInclude.Include.NON_NULL);
		configure(SerializationFeature.WRITE_NULL_MAP_VALUES, true);
		configure(SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
		configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
		configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
		configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
		configure(DeserializationFeature.FAIL_ON_NUMBERS_FOR_ENUMS, false);
		configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		configure(DeserializationFeature.READ_ENUMS_USING_TO_STRING, true);
		configure(SerializationFeature.INDENT_OUTPUT, true);

		configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
//		setAnnotationIntrospector(new JacksonLombokAnnotationIntrospector());

		configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
		configure(JsonParser.Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER, true);

//		registerModule(new JavaTimeModule());
	}
}
