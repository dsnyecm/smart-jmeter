package jmeter.plugins.samplers.smart;

import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;

import java.util.HashSet;
import java.util.Set;

public final class ActionHolder 
{
	private static final Logger log = LoggingManager.getLoggerForClass();

	private String uniqueId, message;
	private long startTime, endTime;
	private boolean error; 
	private String errorMessage;
	private String userName;
	
	private Set<String> destinations = new HashSet<String>();
	
	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public ActionHolder(String uniqueId)
	{
		this.uniqueId = uniqueId;
	}
	
	public ActionHolder(String uniqueId, long startTime, String userName)
	{
		this(uniqueId);
		this.startTime = startTime;
		userName = userName;
		log.debug("Constructing ActionHolder for **************** " + uniqueId);
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public long getStartTime() {
		return startTime;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((uniqueId == null) ? 0 : uniqueId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ActionHolder other = (ActionHolder) obj;
		if (uniqueId == null) {
			if (other.uniqueId != null)
				return false;
		} else if (!uniqueId.equals(other.uniqueId))
			return false;
		return true;
	}

	public void addDestination(String destination) {
		log.debug("******************* Adding destination '" + destination + "' for ActionHolder-" + uniqueId);
		destinations.add(destination);
	}

	public Set<String> getDestinations() {		
		
		return destinations;
	}

	public String getDestinationsAsString() {
		StringBuilder sb = new StringBuilder();
		for (String destination : destinations)
		{
			sb.append(destination).append(", ");
		}
		return sb.toString();
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		log.debug("******************* Setting error message '" + errorMessage + "' for ActionHolder-" + uniqueId);
		this.errorMessage = errorMessage;
	}


	public String getUserName() {
		return userName;
	}

	@Override
	public String toString() {
		return "ActionHolder{" +
				"destinations=" + destinations +
				", endTime=" + endTime +
				", error=" + error +
				", errorMessage='" + errorMessage + '\'' +
				", message='" + message + '\'' +
				", startTime=" + startTime +
				", uniqueId='" + uniqueId + '\'' +
				", userName='" + userName + '\'' +
				'}';
	}
}
