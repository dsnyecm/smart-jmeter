/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmeter.plugins.samplers.smart.websocket.connect;

import jmeter.plugins.samplers.smart.SmartWebSocketSamplerGuiAbstract;

import org.apache.jmeter.testelement.TestElement;


public class SmartWebSocketConnectSamplerGui extends SmartWebSocketSamplerGuiAbstract {

    public SmartWebSocketConnectSamplerGui() {
        super();
    }

    @Override
    public String getStaticLabel() {
        return "Smart WebSocket Connection Sampler";
    }

    @Override
    public TestElement createTestElement() {
    	SmartWebSocketConnectSampler preproc = new SmartWebSocketConnectSampler();
        configureTestElement(preproc);
        return preproc;
    }
}
