/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmeter.plugins.samplers.smart.websocket.subscriber;

import java.lang.reflect.Type;

import org.apache.commons.lang3.StringUtils;
import org.apache.jmeter.samplers.Entry;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.util.MultiValueMap;

import jmeter.plugins.samplers.smart.SmartWebSocketAbstractSampler;


public class SmartWebSocketSubscriberSampler extends SmartWebSocketAbstractSampler 
{
	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggingManager.getLoggerForClass();

    public SmartWebSocketSubscriberSampler() 
    {
        super();
        setName("Smart WebSocket Subscriber Sampler");
    }

    @Override
    protected void initializeHeaders(MultiValueMap<String, String> subscriberHeaders)
    {
    	super.initializeHeaders(subscriberHeaders);
    	subscriberHeaders.add("destination", this.getContextPath());
    	log.debug("Subscription headers:\n" + serializeHeaders(subscriberHeaders));
    }
    
    @Override
    public SampleResult sample(Entry entry) 
    {
    	boolean isOK = true;
    	StringBuilder logMessage = new StringBuilder("\n");
        StringBuilder errorList =  new StringBuilder("\n");
        SampleResult sampleResult = new SampleResult();
        StompHeaders subscriberHeaders = new StompHeaders();
    	
        initializeHeaders(subscriberHeaders);
    	initializeSampleResult(sampleResult, errorList, subscriberHeaders);
    	
        String payloadMessage = getRequestPayload();
        sampleResult.setSamplerData(payloadMessage);

        log.debug("Before subscription........");
        try 
        {
            sampleResult.sampleStart();
            
            
            //if (isIgnoreSslErrors())
			//{
				this.getSubscriberHandler().registerSubscription(getContextPath());
				log.debug("Registering Location subscription: " + getContextPath());
				
			//}
			
        	getStompSession().subscribe(subscriberHeaders, new StompFrameHandler() {
				@Override
				public Type getPayloadType(StompHeaders headers) {
					return String.class;
				}

				@Override
				public void handleFrame(StompHeaders headers, Object payload) {
					if (payload == null)
						payload = "No payload available";
					log.debug("HandleFrame *************\n" + payload.toString());
				}
			});
        
			
        } catch (Exception e) {
            errorList.append(e.getMessage()).append("..........");
            isOK = false;
            log.error(e.getMessage(), e);
        }
        
        log.info("Subscribed to " + this.getContextPath());
        
        sampleResult.sampleEnd();
        sampleResult.setSuccessful(isOK);
        
        sampleResult.setResponseMessage(errorList + "####### Subscribing to " + getContextPath() + " ##########" + logMessage.toString());
        return sampleResult;
    }
   
}
