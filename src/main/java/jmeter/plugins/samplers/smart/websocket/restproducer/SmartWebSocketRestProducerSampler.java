/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmeter.plugins.samplers.smart.websocket.restproducer;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.jmeter.samplers.Entry;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.socket.WebSocketHttpHeaders;

import jmeter.plugins.samplers.smart.SmartWebSocketAbstractSampler;
import jmeter.plugins.samplers.smart.websocket.subscriber.SmartWebSocketSubscriberSessionHandler;


public class SmartWebSocketRestProducerSampler extends SmartWebSocketAbstractSampler
{
	private static final long serialVersionUID = 1L;
	
    private static final Logger log = LoggingManager.getLoggerForClass();
    
    public SmartWebSocketRestProducerSampler() {	
        super();
        setName("Smart WebSocket Rest Producer Sampler");
    }
    
    @Override
    protected void initializeHeaders(MultiValueMap<String, String> headers)
    {
    	super.initializeHeaders(headers);
    	if (StringUtils.isNotBlank(getJSessionId()))
    	{
    		log.debug ("Adding restful producer cookie !!!!!!!!!!!!!!!!" + getJSessionId());
    		headers.add("Cookie", getJSessionId());
    	}
    	headers.add("Content-Type", "application/json");
    }
    
    @Override
    public SampleResult sample(Entry entry) 
    {
    	boolean isOK = false;
    	StringBuilder logMessage = new StringBuilder();
        StringBuilder errorList =  new StringBuilder();
        SampleResult sampleResult = new SampleResult();
        final String uniqueId = UUID.randomUUID().toString();
        WebSocketHttpHeaders restfulHeaders = new WebSocketHttpHeaders();
        Map<String, String[]> expectedLocationsMap = new HashMap<String, String[]>();

    	initializeHeaders(restfulHeaders);
        initializeSampleResult(sampleResult, errorList, restfulHeaders);

        // Add uuid to the body
        String bodyWithId = addUniqueIdToRequest(getRequestPayload(), uniqueId, getContextPath());
        sampleResult.setSamplerData(bodyWithId);

        try 
        {
        	StringBuilder url = new StringBuilder("http://").append(getServerAddressAndPort()).
        			append("/").append(getContextPath());

			HttpEntity<?> httpEntity = new HttpEntity<Object>(bodyWithId.toString(), restfulHeaders);
		
			SmartWebSocketSubscriberSessionHandler stompSubscriberSessionHandler = getSubscriberHandler();
	    	stompSubscriberSessionHandler.addActionHolder(uniqueId, new Date().getTime(), getUserName());
			
	        sampleResult.sampleStart();
	        
	    	ResponseEntity response = new RestTemplate().exchange(url.toString(), HttpMethod.POST, httpEntity, Void.class);
			HttpStatus status = response.getStatusCode();
			Assert.state(status == HttpStatus.OK);

			log.debug("Posted URL [" + url.toString() + "]");
			log.debug("Posted message [" + bodyWithId.toString() + "]");
			log.debug("Response status [" + status.value() + "]");
			
			populateExpectedLocationsMap(expectedLocationsMap);
			
			isOK = getWebSocketResponse(uniqueId, sampleResult, logMessage, errorList, expectedLocationsMap, null);
        	
        } catch (Exception e) 
        {
            errorList.append(e.getMessage()).append("\n").append(StringUtils.join(e.getStackTrace(), "\n")).append("\n");
        }
        
        if (sampleResult.getEndTime() == 0)
        {
        	sampleResult.sampleEnd();
        }
        
        sampleResult.setSuccessful(isOK);

        sampleResult.setResponseMessage(logMessage.toString() + errorList);
        return sampleResult;
    }
  
}
