/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmeter.plugins.samplers.smart.websocket.wsproducer;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.jmeter.samplers.Entry;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;
import org.springframework.messaging.simp.stomp.ConnectionLostException;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.util.MultiValueMap;

import jmeter.plugins.samplers.smart.SmartWebSocketAbstractSampler;
import jmeter.plugins.samplers.smart.websocket.subscriber.SmartWebSocketSubscriberSessionHandler;


public class StompWebSocketProducerSampler extends SmartWebSocketAbstractSampler
{
	private static final long serialVersionUID = 1L;
	
	private static final Logger log = LoggingManager.getLoggerForClass();

    public StompWebSocketProducerSampler() {
        super();
        setName("Smart WebSocket Producer Sampler");
    }
   
    protected void initializeHeaders(MultiValueMap<String, String> producerHeaders)
    {
    	super.initializeHeaders(producerHeaders);
    	producerHeaders.add("destination", this.getContextPath()); 
    }
 
    @Override
    public SampleResult sample(Entry entry) 
    {
    	boolean isOK = true;
    	StringBuilder logMessage = new StringBuilder();
    	StringBuilder errorList =  new StringBuilder();
        final String uniqueId = UUID.randomUUID().toString();
    	StompHeaders producerHeaders = new StompHeaders();
    	Map<String, String[]> expectedLocationsMap = new HashMap<String, String[]>();
    	
    	initializeHeaders(producerHeaders);

        SampleResult sampleResult = new SampleResult();
        try
        {
            initializeSampleResult(sampleResult, logMessage, producerHeaders);

	    	StompSession session = getStompSession();

	    	SmartWebSocketSubscriberSessionHandler stompSubscriberSessionHandler = getSubscriberHandler();
	    	log.debug("Adding ActionHolder for " + uniqueId);

            long startTime = new Date().getTime();
	    	stompSubscriberSessionHandler.addActionHolder(uniqueId, new Date().getTime(), getUserName());
	    	
			String bodyWithId = addUniqueIdToRequest(getRequestPayload(), uniqueId, getContextPath());
            sampleResult.setSamplerData(bodyWithId);
			byte[] array = bodyWithId.getBytes();
			
			 //Could improve procession by moving this closer to the action
	        sampleResult.sampleStart();

            if (!session.isConnected()) {
                String errorMessage = "Session " + session.getSessionId() + " got disconnected, and '" + getContextPath() +
                        " command will not be sent for commandId:" + uniqueId + ", userName:" + getUserName() + " message:\n" + bodyWithId;
                throw new ConnectionLostException(errorMessage);
            }

			session.send(producerHeaders, array);
			log.debug("Sent message: path = '" + getContextPath() + "', body = [" + bodyWithId + "]");

			populateExpectedLocationsMap(expectedLocationsMap);

			isOK = getWebSocketResponse(uniqueId, sampleResult, logMessage, errorList, expectedLocationsMap, session);
        	
        } catch (Throwable e) {
            String text = "********* Error************** while sending command " + uniqueId;
            errorList.append(text).append(e.getMessage()).append(".....");
            log.error(text, e);
            isOK = false;
        }
        
        if (sampleResult.getEndTime() <= 0)	sampleResult.sampleEnd();
        sampleResult.setSuccessful(isOK);
        sampleResult.setResponseMessage(errorList + ", ####### Command ID = " + uniqueId + " #######, " + logMessage.toString());

        return sampleResult;
    }
  
}
