/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmeter.plugins.samplers.smart.websocket.wsproducer;

import jmeter.plugins.samplers.smart.SmartWebSocketSamplerGuiAbstract;

import org.apache.jmeter.testelement.TestElement;

public class WebSocketProducerSamplerGui extends SmartWebSocketSamplerGuiAbstract {

    public WebSocketProducerSamplerGui() {
        super();
    }

    @Override
    public String getStaticLabel() {
        return "Smart WebSocket Producer Sampler";
    }

    @Override
    public TestElement createTestElement() {
    	StompWebSocketProducerSampler preproc = new StompWebSocketProducerSampler();
        configureTestElement(preproc);
        return preproc;
    }
}
