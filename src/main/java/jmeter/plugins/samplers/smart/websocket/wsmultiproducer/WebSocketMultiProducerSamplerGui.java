/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmeter.plugins.samplers.smart.websocket.wsmultiproducer;

import jmeter.plugins.samplers.smart.SmartWebSocketSamplerGuiAbstract;

import org.apache.jmeter.testelement.TestElement;

public class WebSocketMultiProducerSamplerGui extends SmartWebSocketSamplerGuiAbstract {

    public WebSocketMultiProducerSamplerGui() {
        super();
    }

    @Override
    public String getStaticLabel() {
        return "Smart WebSocket Multi-Producer Sampler";
    }

    @Override
    public TestElement createTestElement() {
    	StompWebSocketMultiProducerSampler preproc = new StompWebSocketMultiProducerSampler();
        configureTestElement(preproc);
        return preproc;
    }
}
