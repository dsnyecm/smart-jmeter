/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmeter.plugins.samplers.smart.websocket.connect;

import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.lang3.StringUtils;
import org.apache.jmeter.samplers.Entry;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.ConnectionLostException;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;

import jmeter.plugins.samplers.smart.SmartWebSocketAbstractSampler;
import jmeter.plugins.samplers.smart.websocket.subscriber.SmartWebSocketSubscriberSessionHandler;


public class SmartWebSocketConnectSampler extends SmartWebSocketAbstractSampler {
    
	private static final long serialVersionUID = 1L;
	
	private static final Logger log = LoggingManager.getLoggerForClass();
	
	private WebSocketHttpHeaders connectionHeaders = new WebSocketHttpHeaders();
	
    public SmartWebSocketConnectSampler() {
        super();
        setName("Smart WebSocket Connection Sampler");
    }
    
    private void initializeHeaders()
    {
    	initializeHeaders(connectionHeaders);
		connectionHeaders.add("accept-version", "1.1,1.0");
		connectionHeaders.add("heart-beat", "10000,10000");
    }

    @Override
    public SampleResult sample(Entry entry) 
    {
    	StringBuilder logMessage = new StringBuilder();
    	
        SampleResult sampleResult = new SampleResult();
        sampleResult.setSampleLabel(getName());
        sampleResult.setDataEncoding(getContentEncoding());
        
        //This StringBuilder will track all exceptions related to the protocol processing
        StringBuilder errorList = new StringBuilder();

        boolean isOK = true;

        //Set the message payload in the Sampler
        String payloadMessage = getRequestPayload();
        sampleResult.setSamplerData(payloadMessage);

        try 
        {
        	initializeHeaders();
        	connect(sampleResult);
			logMessage.append("Connection was established....");
        	
        } catch (Exception e) {
			isOK = false;
            errorList.append(e.getMessage());
			log.error(e.getMessage(), e);
        }
        
        sampleResult.sampleEnd();
        sampleResult.setSuccessful(isOK);
        
        sampleResult.setResponseMessage(errorList +  "####### Connecting ##########" +  logMessage.toString());
        return sampleResult;
    }
    
    private void connect(SampleResult sampleResult) throws Exception
    {
    	StandardWebSocketClient sockJsClient = new StandardWebSocketClient();   
		ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
		taskScheduler.afterPropertiesSet();
		registerThreadPoolTaskScheduler(taskScheduler);
		
		String connectionUrl = this.getContextPath();
		WebSocketStompClient stompClient = new WebSocketStompClient(sockJsClient);
		stompClient.setMessageConverter(new MappingJackson2MessageConverter());
		stompClient.setTaskScheduler(taskScheduler);
		stompClient.setDefaultHeartbeat(new long[] {0, 0});
		
		final AtomicReference<Throwable> failure = new AtomicReference<Throwable>();
		SmartWebSocketSubscriberSessionHandler stompSubscriberSessionHandler = new SmartWebSocketSubscriberSessionHandler(failure);
		
		
		//Could improve precision by moving this closer to the action
        sampleResult.sampleStart();

		StompHeaders stompHeaders = new StompHeaders();
		stompHeaders.put("X-XSRF-TOKEN", connectionHeaders.get("X-XSRF-TOKEN"));

		log.info(serializeHeaders(connectionHeaders));
		ListenableFuture<StompSession> future = stompClient.connect(connectionUrl, connectionHeaders, stompHeaders, stompSubscriberSessionHandler);

		
		if (failure.get() != null) {
			AssertionError assertionError =  new AssertionError("Stomp websocket connection failure", failure.get());
			log.error("Stomp websocket connection failure", assertionError);
			throw assertionError;
		}

		StompSession session = null;
		try
		{
			for (int i = 0; i < 10; i++)
			{
				session = future.get();
				if (session == null)
				{
					sleep(1000L);
				}
				else
				{
					log.debug("Connected and registering stompSubscriberSessionHandler");
					session.setAutoReceipt(true);
					registerStompSession(session);
					registerSubscriberHandler(stompSubscriberSessionHandler);
					break;
				}
			}
		}
		catch (Exception e)
		{
			log.error("Stomp websocket connection exception", e);
			AssertionError assertionError =  new AssertionError("Stomp websocket connection exception", e);
			throw assertionError;
		}
		
		if (session != null && session.isConnected())
		{
			log.debug("Connected..........");
		}
		else
		{
			log.debug("Not connected..........");
			throw new ConnectionLostException("Session got disconnected");
		}
    }
}
