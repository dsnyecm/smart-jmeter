/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmeter.plugins.samplers.smart.websocket.wsmultiproducer;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.jmeter.samplers.Entry;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.util.MultiValueMap;

import jmeter.plugins.samplers.smart.SmartWebSocketAbstractSampler;
import jmeter.plugins.samplers.smart.websocket.subscriber.SmartWebSocketSubscriberSessionHandler;


public class StompWebSocketMultiProducerSampler extends SmartWebSocketAbstractSampler
{
	private static final long serialVersionUID = 1L;
	
	private static final Logger log = LoggingManager.getLoggerForClass();

    public StompWebSocketMultiProducerSampler() {
        super();
        setName("Smart WebSocket Multi-Producer Sampler");
    }
   
    protected void initializeHeaders(MultiValueMap<String, String> producerHeaders)
    {
    	super.initializeHeaders(producerHeaders);
    	producerHeaders.add("destination", this.getContextPath()); 
    }
 
    @Override
    public SampleResult sample(Entry entry) 
    {
    	boolean isOK = false;
    	StringBuilder logMessage = new StringBuilder();
    	StringBuilder errorList =  new StringBuilder();
        SampleResult sampleResult = new SampleResult();
    	StompHeaders producerHeaders = new StompHeaders();
        
    	initializeHeaders(producerHeaders);
    	initializeSampleResult(sampleResult, logMessage, producerHeaders);
    	
        //Set the message payload in the Sampler
        String payloadCommand = getRequestPayload();
        
        // Break payload to several messages
        List<String> uniqueIDs = new ArrayList<String>();
        Map<String, String[]> expectedLocationsMap = new HashMap<String, String[]>();
        
        
        try 
        {
	    	StompSession session = getStompSession();
	    	SmartWebSocketSubscriberSessionHandler stompSubscriberSessionHandler = getSubscriberHandler();
        	
	    	//	Could improve procession by moving this closer to the action
	    	sampleResult.sampleStart();
	        for (int i = 1; i < 100; i++)
	        {
	        	String uniqueId = UUID.randomUUID().toString();
	        	uniqueIDs.add(uniqueId);
	        	log.debug("Adding ActionHolder for " + uniqueId);
	        	stompSubscriberSessionHandler.addActionHolder(uniqueId, new Date().getTime(), getUserName());
	    	
	        	String bodyWithId = addUniqueIdToRequest(payloadCommand, uniqueId, getContextPath());
	        	bodyWithId = bodyWithId.replaceAll("random", String.valueOf(i));
	        	sampleResult.setSamplerData(bodyWithId);
	        	byte[] array = bodyWithId.getBytes();
			
	        	session.send(producerHeaders, array);
	        	log.debug("Sent message [" + bodyWithId + "]");
	        	sleep(2);
	        }
	        
	        populateExpectedLocationsMap(expectedLocationsMap);
			
	        for (String uniqueId : uniqueIDs)
	        {
	        	isOK = getWebSocketResponse(uniqueId, sampleResult, logMessage, errorList, expectedLocationsMap, session);
	        	if (!isOK)
	        		break;
	        }
        	
        } catch (Exception e) {
            errorList.append(e.getMessage()).append("\n").append(StringUtils.join(e.getStackTrace(), "\n")).append("\n");
        }
        
        if (sampleResult.getEndTime() == 0)
        {
        	sampleResult.sampleEnd();
        }
        
        sampleResult.setSuccessful(isOK);

        sampleResult.setResponseMessage(logMessage.toString() + errorList);
        return sampleResult;
    }
  
}
