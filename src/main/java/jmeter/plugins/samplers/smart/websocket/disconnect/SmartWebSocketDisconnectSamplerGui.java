/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmeter.plugins.samplers.smart.websocket.disconnect;

import jmeter.plugins.samplers.smart.SmartWebSocketSamplerGuiAbstract;

import org.apache.jmeter.testelement.TestElement;


public class SmartWebSocketDisconnectSamplerGui extends SmartWebSocketSamplerGuiAbstract {

    public SmartWebSocketDisconnectSamplerGui() {
        super();
    }

    @Override
    public String getStaticLabel() {
        return "Smart WebSocket Disconnection Sampler";
    }

    @Override
    public TestElement createTestElement() {
    	SmartWebSocketDisconnectSampler preproc = new SmartWebSocketDisconnectSampler();
        configureTestElement(preproc);
        return preproc;
    }
}
