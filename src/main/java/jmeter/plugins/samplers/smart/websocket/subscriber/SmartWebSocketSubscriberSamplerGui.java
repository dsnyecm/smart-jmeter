/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmeter.plugins.samplers.smart.websocket.subscriber;

import org.apache.jmeter.testelement.TestElement;

import jmeter.plugins.samplers.smart.SmartWebSocketSamplerGuiAbstract;

public class SmartWebSocketSubscriberSamplerGui extends SmartWebSocketSamplerGuiAbstract {

    public SmartWebSocketSubscriberSamplerGui() {
        super();
    }

    @Override
    public String getStaticLabel() {
        return "Smart WebSocket Subscriber Sampler";
    }

 
    @Override
    public TestElement createTestElement() {
    	SmartWebSocketSubscriberSampler preproc = new SmartWebSocketSubscriberSampler();
        configureTestElement(preproc);
        return preproc;
    }

}
