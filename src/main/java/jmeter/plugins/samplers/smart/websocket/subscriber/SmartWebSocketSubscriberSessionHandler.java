package jmeter.plugins.samplers.smart.websocket.subscriber;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import jmeter.plugins.samplers.smart.ActionHolder;
import jmeter.plugins.samplers.smart.SMARTObjectMapper;
import org.apache.jmeter.util.JMeterUtils;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;

public class SmartWebSocketSubscriberSessionHandler extends StompSessionHandlerAdapter implements Serializable
{
	private static final Logger logger = LoggingManager.getLoggerForClass();
	
	private final AtomicReference<Throwable> failure;
	private final Map<String, ActionHolder> actionHolders = new ConcurrentHashMap<>();
	private HashSet<String>  subscribedLocations = new HashSet<String>();
	private ObjectMapper jacksonObjectMapper = new SMARTObjectMapper() ;
	
	public SmartWebSocketSubscriberSessionHandler (AtomicReference<Throwable> failure)
	{
		this.failure = failure;
	}
	
	@Override
	public void handleException(StompSession s, StompCommand c, StompHeaders h, byte[] p, Throwable ex) 
	{

		String message = new String(p).toString();
		String destination = h.getDestination().trim();

		JsonNode root = null;
		String uniqueId = null;
		try {
			root = jacksonObjectMapper.readTree(message);
			JsonNode node = root.get("commandId");
			if (node != null) {
				uniqueId = node.asText();
				logger.debug("********* Extracted commandId -->>>>>>>>>>> " + uniqueId);
			}
		}
		catch (Exception e)
		{
			logger.error("Cant load jason from string", e);
		}

	    if (uniqueId != null)
	    {
			logger.debug("************** ReceivedMessageCommandId '" + uniqueId + "' From Destination: " + destination + "\n" + message);

			long endTime = System.currentTimeMillis();
			ActionHolder ah = getActionHolder(uniqueId, message, endTime, destination);
			if (ah != null)
			{
				logger.debug("********* ActionHolder was found for commandId = " + uniqueId);
				finalizeActionHolderState(ah, message, endTime, destination);
			}
			else
			{
				logger.debug("******************* ActionHolder was not created by this client for commandId = " + uniqueId);
			}
	    }
	    else
	    {
	    	logger.debug("********* Could not extract commandId from message {" + message + " }");
	    }
		return;
	}

	public static void finalizeActionHolderState(ActionHolder ah, String message, long endTime, String destination)
	{
		if (ah.getEndTime() <= 0) {
			ah.setEndTime(endTime);
		}

		if (ah.getMessage() == null) {
			ah.setMessage(message);
		}

		if (message.indexOf("ErrorEvent") >= 0)
		{
			logger.error("!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Error event came back");
			ah.setError(true);
			ah.setErrorMessage("Error event came back");
		}
		else if (message.indexOf("NoOpEvent") >= 0)
		{
			logger.debug("!!!!!!!!!!!!!!!!!!!!!!!!!!!!! NoOp event came back");
			ah.setError(true);
			ah.setErrorMessage("NoOp event came back");
		}


		if (!ah.isError())
		{
			if (ah.getDestinations().contains(destination))
			{
				String  text = "ReceivedAnExtraMessage for commandId '" + ah.getUniqueId() + "' from " + destination;
				logger.warn(text);
//				ah.setError(true);
//				ah.setErrorMessage(text);
			}
			else
			{
				logger.debug("********* Received message from " + destination);
				ah.addDestination(destination);
			}
		}

	}
	
	public ActionHolder getActionHolder(String uniqueId, String message, long endTime, String destination)
	{
		logger.debug("getActionHolder request came for " + uniqueId);
		logger.debug("Local ActionHolder map size is " + actionHolders.size());
		ActionHolder ah = actionHolders.get(uniqueId.trim());
		if (ah != null) {
			if (ah.getMessage() == null)
				ah.setMessage(message);

			if (ah.getEndTime() <= 0)
				ah.setEndTime(endTime);

			if (destination != null)
				ah.addDestination(destination);

			return ah;
		}

		// Store messages receeved by another thread;
		String propertyValue = JMeterUtils.getProperty(uniqueId);
		if ( propertyValue != null)
		{
			logger.debug("********************* Received message which belongs to another thread: (" + message + ")");
			ah = new ActionHolder(uniqueId);
			ah.setMessage(message);
			ah.setEndTime(endTime);
			ah.addDestination(destination);
			if (message != null) {
				int counter = getNextCounter(uniqueId);
				StringBuilder sb = new StringBuilder(message);
				sb.append(getPropertyDelimiter()).append(String.valueOf(endTime))
						.append(getPropertyDelimiter()).append(destination);
				JMeterUtils.setProperty(getPropertyName(uniqueId, counter), sb.toString());
				logger.debug("setProperty ' " + getPropertyName(uniqueId, counter) + " to { " + sb.toString() + " }");
			}
		}

		return ah;
	}

	public void addActionHolder(String uniqueId, long startTime, String userName)
	{
		actionHolders.put(uniqueId.trim(), new ActionHolder(uniqueId, startTime, userName));
		JMeterUtils.setProperty(uniqueId, uniqueId);
	}

	public static String getPropertyName(String uniqueId, int counter)
	{
		return "command-" + counter + "-" + uniqueId;
	}

	public static String getPropertyDelimiter()
	{
		return "#####";
	}

	private static int getNextCounter(String uniqueId)
	{
		int counter = 1;
		while (true) {
			String propertyValue = JMeterUtils.getProperty(getPropertyName(uniqueId, counter));
			if (propertyValue == null)
				return counter;
			else
				counter += 1;
		}
	}

	// ************************ Overriding other error conditions ********************************************
	@Override
	public void handleFrame(StompHeaders headers, Object payload) {
		logger.warn("handleFrame *********************************************" + payload.toString());
	}

	@Override
	public void afterConnected(final StompSession session, StompHeaders connectedHeaders) 
	{
		logger.debug("afterConnected....................................");
	}
	

	@Override
	public void handleTransportError(StompSession session, Throwable exception) {
		logger.debug("handleTransportError....................................", exception);
	}

	public static Logger getLogger() {
		return logger;
	}

	public AtomicReference<Throwable> getFailure() {
		return failure;
	}

	public Set<String> getSubscribedLocations(Map<String, String[]> expectedLocationsMap) {
		Set<String> requestedLocations = new TreeSet<String>();
		for (String domain : expectedLocationsMap.keySet())
		{
			String [] locations = expectedLocationsMap.get(domain);
			for (String location : locations)
			{
				for (String subscribedLocation : subscribedLocations)
				{
					if (subscribedLocation.contains(location) && subscribedLocation.contains(domain))
					{
						logger.debug("*********adding to requestedLocation::"+location);
						requestedLocations.add(location);
					}
				}
			}
		}
		return requestedLocations;
	}

	public void registerSubscription(String subscribedLocation) {
		subscribedLocations.add(subscribedLocation);
	}


}
