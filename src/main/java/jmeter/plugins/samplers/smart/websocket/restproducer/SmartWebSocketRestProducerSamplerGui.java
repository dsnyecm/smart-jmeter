/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmeter.plugins.samplers.smart.websocket.restproducer;

import jmeter.plugins.samplers.smart.SmartWebSocketSamplerGuiAbstract;

import org.apache.jmeter.testelement.TestElement;

public class SmartWebSocketRestProducerSamplerGui extends SmartWebSocketSamplerGuiAbstract {

    public SmartWebSocketRestProducerSamplerGui() {
        super();
    }

    @Override
    public String getStaticLabel() {
        return "Smart WebSocket Rest Producer Sampler";
    }

    @Override
    public TestElement createTestElement() {
    	SmartWebSocketRestProducerSampler preproc = new SmartWebSocketRestProducerSampler();
        configureTestElement(preproc);
        return preproc;
    }
}
