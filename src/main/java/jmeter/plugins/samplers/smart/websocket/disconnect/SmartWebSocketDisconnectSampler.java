/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmeter.plugins.samplers.smart.websocket.disconnect;

import org.apache.commons.lang3.StringUtils;
import org.apache.jmeter.samplers.Entry;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import jmeter.plugins.samplers.smart.SmartWebSocketAbstractSampler;


public class SmartWebSocketDisconnectSampler extends SmartWebSocketAbstractSampler {
    
	private static final long serialVersionUID = 1L;
	
	private static final Logger log = LoggingManager.getLoggerForClass();
	
	
	
    public SmartWebSocketDisconnectSampler() {
        super();
        setName("Smart WebSocket Disconnection Sampler");
    }
    
    @Override
    public SampleResult sample(Entry entry) 
    {
    	StringBuilder logMessage = new StringBuilder();
    	
        SampleResult sampleResult = new SampleResult();
        sampleResult.setSampleLabel(getName());
        sampleResult.setDataEncoding(getContentEncoding());
        
        //This StringBuilder will track all exceptions related to the protocol processing
        StringBuilder errorList = new StringBuilder();
        errorList.append("\n\n[Problems]\n");
        
        boolean isOK = true;

        //Set the message payload in the Sampler
        String payloadMessage = getRequestPayload();
        sampleResult.setSamplerData(payloadMessage);

        try 
        {
        	disconnect(sampleResult);
        	
        } 
        catch (Exception e) 
        {
            errorList.append(e.getMessage()).append("\n").append(StringUtils.join(e.getStackTrace(), "\n")).append("\n");
        }
        
        sampleResult.sampleEnd();
        sampleResult.setSuccessful(isOK);
        
        sampleResult.setResponseMessage(logMessage.toString() + errorList);
        return sampleResult;
    }
    
    private void disconnect(SampleResult sampleResult)
    {
		sampleResult.sampleStart();
	
		try
		{
			StompSession session = getStompSession();
			session.disconnect();
		}
		catch (Exception e)
		{
			log.error("Disconnection error !!!!!!!!!!!!!!!!!!!!!!!!!!!!! " + e.getMessage());
		}
		finally 
		{
			log.error("Disconnected session ***************************");
		}

		
		
		try
		{
			ThreadPoolTaskScheduler taskScheduler = getThreadPoolTaskScheduler();
			taskScheduler.shutdown();
		}
		catch (Exception e){}
				
		registerStompSession(null);
		registerThreadPoolTaskScheduler(null);
		registerSubscriberHandler(null);
    }		
}
