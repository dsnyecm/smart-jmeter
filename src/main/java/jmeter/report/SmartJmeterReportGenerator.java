package jmeter.report;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.StringWriter;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.ProxySelector;
import java.net.SocketAddress;
import java.net.URI;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PushCommand;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.RefSpec;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public final class SmartJmeterReportGenerator {

	private final static String[] parametersArray = new String[]{"reportPath", "localGitPath", "jMeterScriptName",
		"proxyServer", "proxyPort", "gitLogin", "gitPassword", "pushToGit" };
	private final static List<String> PARAMETERS_LIST = new ArrayList<String>(Arrays.asList(parametersArray));
	
	private final static String DEFAULT_REPORT_PATH = "C:/smart-jmeter/jmeter/build/jmeter-report";
	private final static String DEFAULT_LOCAL_GIT_PATH = "C:/smart-jmeter";
	private final static String DEFAULT_PROXY_SERVER = "bcpxy.nycnet";
	private final static Integer DEFAULT_PROXY_PORT = 8080;
	private final static String DEFAULT_REMOTE_GIT_LOGIN_ID = "ipapkovdsny";
	private final static String DEFAULT_REMOTE_GIT_PASSWORD = "Moscow123";
	private final static String DEFAULT_JMETER_SCRIPT_NAME = "baseline";
	private final static Boolean DEFAULT_PUSH_TO_GIT = true;
	
	
	private final static String[] xmlAttributesArray = new String[]{"lb", "t" };
	private final static List<String> ATTRIBUTES_LIST = new ArrayList<String>(Arrays.asList(xmlAttributesArray));
	
	public static void main(String[] args) 
	{
		// Populate map with input parameters
		final Map<String, String> parametersMap = new HashMap<String, String>();
		if (args.length > 0)
		{
			for (String arg : args)
			{
				String[] keyValue = arg.split("=");
				if (PARAMETERS_LIST.contains(keyValue[0].trim()))
				{
					parametersMap.put(keyValue[0], keyValue[1]);
				}
			}
		}
		
		try 
		{
			// Set local variables according to input parameters
			final String reportPath = parametersMap.get(parametersArray[0]) == null? DEFAULT_REPORT_PATH : parametersMap.get(parametersArray[0]);
			final String localGitPath = parametersMap.get(parametersArray[1]) == null? DEFAULT_LOCAL_GIT_PATH : parametersMap.get(parametersArray[1]);
			final String jmeterScriptName = parametersMap.get(parametersArray[2]) == null? DEFAULT_JMETER_SCRIPT_NAME : parametersMap.get(parametersArray[2]);
			final String proxyServer = parametersMap.get(parametersArray[3]) == null? DEFAULT_PROXY_SERVER : parametersMap.get(parametersArray[3]);
			final Integer proxyPort = parametersMap.get(parametersArray[4]) == null? DEFAULT_PROXY_PORT : Integer.valueOf(parametersMap.get(parametersArray[4]));
			final String loginId = parametersMap.get(parametersArray[5]) == null? DEFAULT_REMOTE_GIT_LOGIN_ID : parametersMap.get(parametersArray[5]);
			final String password = parametersMap.get(parametersArray[6]) == null? DEFAULT_REMOTE_GIT_PASSWORD : parametersMap.get(parametersArray[6]);
			final Boolean pushToGit = parametersMap.get(parametersArray[7]) == null? DEFAULT_PUSH_TO_GIT : Boolean.valueOf(parametersMap.get(parametersArray[7]));

			Map<String, Sample> samples = new HashMap<String, Sample>();
			
			// load olderst file
			File baseLineFile =  findReportXml(reportPath, jmeterScriptName, true);
			loadXmlReport (baseLineFile, samples,  true);
			
			// Load newest file
			File latestFile =  findReportXml(reportPath, jmeterScriptName, false);
			Document doc = loadXmlReport (latestFile, samples,  false);
			
			// Add regressions to document
			addRegressionsToDocument(doc, latestFile, samples);
			
			// Generate HTML
			String htmlFileName = generateHtml(reportPath, doc, latestFile, baseLineFile);
			
			// Push html report to GIT
			if (pushToGit.booleanValue())
				pushReportToGit(localGitPath, htmlFileName, proxyServer, proxyPort, loginId, password);
			
			System.out.println("Generated report " + htmlFileName);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	public static File findReportXml(String directoryPath, String jmeterScriptName, boolean isBaseLine) 
	{
	    File fl = new File(directoryPath);
	    File[] files = fl.listFiles(new FileFilter() 
	    {          
	        public boolean accept(File file) {
	            return file.isFile();
	        }
	    });
	    
	    long lastMod = Long.MIN_VALUE;
	    File choice = null;
	    for (File file : files) 
	    {
	    	
	    	if (file.getName().endsWith("xml") && file.getName().contains(jmeterScriptName))
	    	{
	    		if (isBaseLine)
	    		{
	    			if (lastMod == Long.MIN_VALUE ||  file.lastModified() < lastMod) {
	    				choice = file;
	    				lastMod = file.lastModified();
	    			}
	    		}
	    		else
	    		{
	    			if (file.lastModified() > lastMod) {
	    				choice = file;
	    				lastMod = file.lastModified();
	    			}
		        }
	    	}
	    }
	    return choice;
	}
	
	private static Document loadXmlReport (File file, Map<String, Sample> samples,  boolean isBbaseLine) throws Exception
	{
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(file);
		
		NodeList nl = doc.getDocumentElement().getChildNodes();
	    if(nl.getLength() > 0) 
	    {
	    	int size = nl.getLength();
	        for (int i = 0; i < size; i++) 
	        {
	        	addSample(nl.item(i), samples, isBbaseLine);
	        }
	    }
	    
	    return doc;
	}
	
	private static void addSample(Node node, Map<String, Sample> samples, boolean baseline)
	{
		NamedNodeMap attributes = node.getAttributes();
		if (attributes == null)
			return;
		
		int numAttrs = attributes.getLength();
		if (numAttrs > 0)
		{
			Sample sample = null;
			for (int i = 0; i < numAttrs; i++) 
			{
				Attr attr = (Attr) attributes.item(i);
				String attrName = attr.getNodeName();
				String attrValue = attr.getNodeValue().trim();
				
			    if (!ATTRIBUTES_LIST.contains(attrName))
			    	continue;
			    
			    if (attrName.equals(xmlAttributesArray[0]))
			    {
			    	sample = samples.get(attrValue);
					if (sample == null)
					{
						sample = new Sample(attrValue);
						samples.put(attrValue, sample);
					}
			    }
				else if (attrName.equals(xmlAttributesArray[1]))			    
				{
			    	sample.addTime(attr.getNodeValue(), baseline);
			    	sample = null;
			    }
			}
		}

	}
	
	private static Document addRegressionsToDocument(Document document, File originalReportFile, Map<String, Sample> samples) throws Exception
	{
		NodeList nl = document.getDocumentElement().getChildNodes();
	    if(nl.getLength() > 0) 
	    {
	    	int size = nl.getLength();
	        for (int i = 0; i < size; i++) 
	        {
	        	addRegressionAttribute(nl.item(i), samples);
	        }
	    }
	    
	    return document;	
	}
	
	private static void addRegressionAttribute(Node node, Map<String, Sample> samples)
	{
		NamedNodeMap attributes = node.getAttributes();
		if (attributes == null)
			return;
		
		int numAttrs = attributes.getLength();
		if (numAttrs > 0)
		{
			for (int i = 0; i < numAttrs; i++) 
			{
	        	Attr attr = (Attr) attributes.item(i);
				String attrName = attr.getNodeName();
				String attrValue = attr.getNodeValue().trim();
					
				if (!ATTRIBUTES_LIST.contains(attrName))
					continue;
		        	
		        Element element = (Element) node;
		        Sample sample = samples.get(attrValue);
		        element.setAttribute("avgrt", String.valueOf(sample.getRegressionTime()));
		        break;
			}
		}
	    
		return;
	}
	
	private static String generateHtml(String directoryPath, Document document,
			File originalReportFile, File baseLineReportFile) throws Exception
	{
		File stylesheet = new File(directoryPath + "/jmeter-comparison-report.xsl");
        StreamSource stylesource = new StreamSource(stylesheet);
        
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer xsltTransformer = transformerFactory.newTransformer(stylesource);
		xsltTransformer.setOutputProperty("indent","yes");
		xsltTransformer.setParameter("dateReport", new Date());
		xsltTransformer.setParameter("originalReport", originalReportFile.getName());
		xsltTransformer.setParameter("baselineReport", baseLineReportFile.getName());
		
		DOMSource xmlSource=new DOMSource(document); 
		
		StringWriter stringWriter=new StringWriter();
	    StreamResult result = new StreamResult(stringWriter);

		xsltTransformer.transform(xmlSource, result);
		
		String htmlFileName = originalReportFile.getAbsolutePath().replace("xml", "html");
		FileUtils.writeStringToFile(new File(htmlFileName), stringWriter.toString());
		
		return htmlFileName;
	}
	
	
	private static void pushReportToGit(String localGitPath, String htmlFilePath, 
			final String proxyServer, final Integer proxyPort, String loginId, String password) throws Exception
	{
		// Set proxy
		ProxySelector.setDefault(new ProxySelector() 
		{
            @Override
            public List<Proxy> select(URI uri) 
            {
                return Arrays.asList(new Proxy(Type.HTTP, InetSocketAddress
                            .createUnresolved(proxyServer, proxyPort.intValue())));
            }

            @Override
            public void connectFailed(URI uri, SocketAddress sa, IOException ioe) {
                if (uri == null || sa == null || ioe == null) {
                    throw new IllegalArgumentException(
                            "Arguments can't be null.");
                }
            }
        });

		
		// Push new report to git
		Repository localRepo = new FileRepository(localGitPath + "/.git");
        Git git = new Git(localRepo);
		
        git.add().addFilepattern(".").call();
        git.commit().setMessage("Added new report: " + htmlFilePath).call();
        
        PushCommand pushCommand = git.push();
        pushCommand.setCredentialsProvider(new UsernamePasswordCredentialsProvider( loginId, password));
        pushCommand.setForce(true);
        pushCommand.setRemote("origin");
        pushCommand.setPushAll();
        
        String refSpecString="refs/heads/{0}:refs/heads/{0}";
        refSpecString=MessageFormat.format(refSpecString,"master");
        RefSpec spec=new RefSpec(refSpecString);
        pushCommand.setRefSpecs(spec);
        
        pushCommand.call();
	}

	private static class Sample
	{
		private String name;
		private ReportData baselineReportData = new ReportData();
		private ReportData latestReportData = new ReportData();
		
		
		public Sample(String name) 
		{
			super();
			this.name = name;
		}
		
		public void addTime(String ms, boolean isBaseLine)
		{
			if (isBaseLine)
				baselineReportData.values.add(Integer.valueOf(ms));
			else
				latestReportData.values.add(Integer.valueOf(ms));
			return;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Sample other = (Sample) obj;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			return true;
		}

		@Override
		public String toString() 
		{
			populateValues(baselineReportData);
			populateValues(latestReportData);
			
			StringBuilder sb = new StringBuilder("Sample [name=");
			sb.append(name).append(", min=").append(baselineReportData.min).append(":").append(latestReportData.min)
			.append(", max=").append(baselineReportData.max).append(":").append(latestReportData.max)
			.append(", avg=").append(baselineReportData.avg).append(":").append(latestReportData.avg)
			.append(", hits=").append(latestReportData.values.size() + 2).append("]");
			
			return sb.toString();

		}
		
		public long getRegressionTime()
		{
			populateValues(baselineReportData);
			populateValues(latestReportData);
			return latestReportData.avg - baselineReportData.avg;
		}
		
		private void populateValues (ReportData reportData)
		{
			Collections.sort(reportData.values);
			int size = reportData.values.size();
			if (size > 3)
			{
				reportData.values.remove(size - 1);
				reportData.values.remove(0);
			}
			
			size = reportData.values.size();
			if (size == 0)
				size = 1;
			for (Integer value : reportData.values)
			{
				if (reportData.min == null)
				{
					reportData.min = value;
				}
				else
				{
					reportData.max = value;
				}
				reportData.avg += value;
			}
			reportData.avg /=size;
			return;
		}
	}
	
	private static class ReportData
	{
		public List<Integer> values = new ArrayList<Integer>();
		public Integer min = null;
		public Integer max = null;
		public Integer avg = new Integer(0);
	}
}
